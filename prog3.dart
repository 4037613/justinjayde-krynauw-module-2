void main() {
  Mtn app_one = new Mtn("The Ambani Africa App", "Best 'South African' App",
      "Ambani Africa", 2021);
  String name = app_one.name;
  String cat = app_one.category;
  String dev = app_one.developer;
  int year = app_one.year;
  print("Name: $name,  Category:  $cat, Developer: $dev, Year: $year");
  app_one.display();
}

class Mtn {
  String name = "";
  String category = "";
  String developer = "";
  int year = 0;

  Mtn(String name, String category, String developer, int year) {
    this.name = name;
    this.category = category;
    this.developer = developer;
    this.year = year;
  }

  void display() {
    this.name = this.name.toUpperCase();
    print(this.name);
  }
}
